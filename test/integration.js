/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const uuid = require('uuid');

const BASE_URL = 'http://localhost:3000';

const clickHome = () => $('a[href="/"]').click();
const clickNotes = () => $('a[href="/notes"]').click();
const clickTopics = () => $('a[href="/topics"]').click();

const setUser = (user) => {
  // Set user to provided user id if provided or random guid otherwise
  const userId = user || `user-${uuid.v4()}`;

  // Navigate to the home page and set the user input
  clickHome();
  $('.userInput').setValue(userId);

  return userId;
};

const createNote = (noteTitle, noteBody) => {
  clickNotes();

  // Create note and wait for the new note element to appear in the DOM
  $('.createNote').click();
  $('.note').waitForExist({ timeout: 3000 });

  // Start editing newly created note
  const noteIdx = $$('.note').length - 1;
  const noteId = $$('.note')[noteIdx].getAttribute('id');
  $(`#${noteId} .editNote`).waitForExist({ timeout: 3000 });
  $(`#${noteId} .editNote`).click();

  // Enter note title and body
  $(`#${noteId} .noteTitleInput`).setValue(noteTitle);
  $(`#${noteId} .noteBodyInput`).setValue(noteBody);

  // Stop editing the note
  $(`#${noteId} .editNote`).click();
  $('.noteBody').waitForExist({ timeout: 3000 });

  return noteId;
};

describe('Integration Tests', () => {
  beforeEach(() => {
    browser.url('');
  });

  describe('Navigation', () => {
    it('should navigate between pages', () => {
      browser.url('');
      expect(browser.getUrl()).to.equal(`${BASE_URL}/`);

      clickTopics();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/topics`);

      clickNotes();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/notes`);

      clickHome();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/`);
    });

    it('should navigate between topics', () => {
      browser.url('/topics');
      expect(browser.getUrl()).to.equal(`${BASE_URL}/topics`);

      $('#topic2').click();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/topics/topic2`);

      $('#topic1').click();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/topics/topic1`);
    });
  });

  describe('Notes', () => {
    beforeEach(() => {
      clickNotes();
      $('.deleteNotes').click();
    });

    it('should create and delete notes', () => {
      setUser();

      clickNotes();
      expect(browser.getUrl()).to.equal(`${BASE_URL}/notes`);

      $('.createNote').click();
      expect($('.editNote')).to.exist;
      expect($$('.noteTitle')[0].getText()).to.equal('');
      expect($$('.noteBody')[0].getText()).to.equal('');

      $('.deleteNotes').click();
      expect($$('.editNote').length).to.equal(0);
    });

    it('should edit notes', () => {
      setUser();

      const note1Id = createNote('note 1 title', 'note 1 body');
      const note2Id = createNote('', 'note 2 body');

      expect($(`#${note1Id} .noteTitle`).getText()).to.equal('note 1 title');
      expect($(`#${note1Id} .noteBody`).getText()).to.equal('note 1 body');
      expect($(`#${note2Id} .noteTitle`).getText()).to.equal('');
      expect($(`#${note2Id} .noteBody`).getText()).to.equal('note 2 body');

      // Edit note 2
      $(`#${note2Id} .editNote`).click();

      // Change note 2's title
      $(`#${note2Id} .noteTitleInput`).setValue('note 2 title');

      expect($(`#${note2Id} .noteTitleInput`).getValue()).to.equal('note 2 title');
      expect($(`#${note2Id} .noteBodyInput`).getValue()).to.equal('note 2 body');
      // expect($$('.noteTitleInput')[1].getValue()).to.equal('note 2 title');
      // expect($$('.noteBodyInput')[1].getValue()).to.equal('note 2 body');

      // Stop editing note 2
      $(`#${note2Id} .editNote`).click();
      expect($(`#${note1Id} .noteTitle`).getText()).to.equal('note 1 title');
      expect($(`#${note1Id} .noteBody`).getText()).to.equal('note 1 body');
      expect($(`#${note2Id} .noteTitle`).getText()).to.equal('note 2 title');
      expect($(`#${note2Id} .noteBody`).getText()).to.equal('note 2 body');
      // expect($$('.noteTitle')[0].getText()).to.equal('note 1 title');
      // expect($$('.noteBody')[0].getText()).to.equal('note 1 body');
      // expect($$('.noteTitle')[1].getText()).to.equal('note 2 title');
      // expect($$('.noteBody')[1].getText()).to.equal('note 2 body');
    });

    it('should save notes when changing users', () => {
      console.log('save test <<<');
      const user1 = setUser();
      console.log(`user1::: ${user1}`);
      const user1Note1Id = createNote('User 1 note 1 title', '');
      const user1Note2Id = createNote('', 'User 1 note 2 body');
      console.log(`user1 num notes::: ${$('.note')}`);

      const user2 = setUser();
      const user2NoteId = createNote('User 2 note title', 'User 2 note 2 body');

      // Go back to the first user and ensure that users's notes are still present
      setUser(user1);
      clickNotes();
      $('.note').waitForExist({ timeout: 3000 });
      console.log(`notes length:: ${$$('.note').length}`);

      expect($(`#${user1Note1Id} .noteTitle`).getText()).to.equal('User 1 note 1 title');
      expect($(`#${user1Note1Id} .noteBody`).getText()).to.equal('');
      expect($(`#${user1Note2Id} .noteTitle`).getText()).to.equal('');
      expect($(`#${user1Note2Id} .noteBody`).getText()).to.equal('User 1 note 2 body');

      // Go back to the second user and ensure that users's notes are still present
      setUser(user2);
      clickNotes();

      expect($(`#${user2NoteId} .noteTitle`).getText()).to.equal('User 2 note title');
      expect($(`#${user2NoteId} .noteBody`).getText()).to.equal('User 2 note 2 body');
    });
  });
});
