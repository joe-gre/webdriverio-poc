import React from 'react';
import PropTypes from 'prop-types';

export default function Home(props) {
  const { user, setUser } = props;

  return (
    <div>
      <h2>Home</h2>
      <h3>User</h3>
      <p>Enter a user to see notes belonging to that user</p>
      <input
        className="userInput"
        value={user}
        onChange={event => setUser(event.target.value)}
        type="text"
        placeholder="User"
      />
    </div>
  );
}

Home.propTypes = {
  user: PropTypes.string.isRequired,
  setUser: PropTypes.func.isRequired
};
