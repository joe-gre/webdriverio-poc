import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import debounce from 'debounce';
import Note from './Note';

const saveNotes = (notes, user) => {
  axios.post(`/api/setNotes?user=${user}`, {
    notes
  });
};

const debouncedSaveNotes = debounce(saveNotes, 100);

export default function Notes(props) {
  const { user, setError } = props;
  const [notes, setNotes] = useState([]);

  const setNote = (changedNote) => {
    const updatedNotes = notes.map((curNote) => {
      if (curNote.id === changedNote.id) {
        return changedNote;
      }

      return curNote;
    });

    debouncedSaveNotes(updatedNotes, user);
    setNotes(updatedNotes);
  };

  useEffect(() => {
    axios.get(`/api/getNotes?user=${user}`).then((res) => {
      setNotes(res.data.notes ? res.data.notes : []);
    });
  }, [user]);

  return (
    <div className="notesContainer">
      <h2>Notes</h2>
      {notes ? notes.map(note => (
        <Note id={note.id} note={note} setNote={setNote} setError={setError} />
      )) : ''}
      <button
        type="button"
        className="createNote"
        onClick={() => {
          const updatedNotes = notes.concat([{
            id: uuidv4(),
            title: '',
            body: ''
          }]);
          setNotes(updatedNotes);
        }}
      >
        Create Note
      </button>
      <button
        type="button"
        className="deleteNotes"
        onClick={() => {
          axios.post(`/api/deleteNotes?user=${user}`);
          setNotes([]);
        }}
      >
        Delete Notes
      </button>
    </div>
  );
}

Notes.propTypes = {
  user: PropTypes.string.isRequired,
  setError: PropTypes.func.isRequired
};
