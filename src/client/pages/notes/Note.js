import React, { useState } from 'react';
import PropTypes from 'prop-types';

export default function Note(props) {
  const { note, setNote } = props;
  const [isEditing, setIsEditing] = useState(false);

  return (
    <div id={`note-${note.id}`} className="note">
      {isEditing ? (
        <div>
          <h3 className="inputName">Title</h3>
          <input
            className="noteTitleInput"
            value={note.title}
            onChange={(event) => {
              setNote({
                ...note,
                title: event.target.value
              });
            }}
            type="text"
            placeholder="Note title"
          />
          <h3 className="inputName">Body</h3>
          <textarea
            className="noteBodyInput"
            value={note.body}
            onChange={(event) => {
              setNote({
                ...note,
                body: event.target.value
              });
            }}
            placeholder="Note body"
          />
        </div>
      ) : (
        <div>
          <h3 className="noteTitle">{note.title}</h3>
          <p className="noteBody">{note.body}</p>
        </div>
      )}

      <button className="editNote" type="button" onClick={() => setIsEditing(!isEditing)}>
        {isEditing ? 'Close' : 'Edit'}
      </button>

    </div>
  );
}

Note.propTypes = {
  note: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    body: PropTypes.string,
  }).isRequired,
  setNote: PropTypes.func.isRequired
};
