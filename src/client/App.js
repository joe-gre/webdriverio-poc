import React, { useState, useEffect } from 'react';
import './app.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import axios from 'axios';
import Home from './pages/home/Home';
import Topics from './pages/topic/Topics';
import Notes from './pages/notes/Notes';

export default function App() {
  const [user, setUser] = useState('');
  const [error, setError] = useState('');

  useEffect(() => {
    axios.get('/api/getUsername')
      .then(res => setUser(res.data.username));
  }, []);

  return (
    <Router>
      <div className="header">
        {error ? (
          <div className="error">
            <h2>Error when saving:</h2>
            <h3>{error}</h3>
          </div>
        ) : ''}
        <div className="greeting">
          {user ? <h1>{`Hello ${user}`}</h1> : <h1>Loading.. please wait!</h1>}
        </div>
        <div className="nav">
          <Link to="/">Home</Link>
          <Link to="/notes">Notes</Link>
          <Link to="/topics">Topics</Link>
        </div>
      </div>
      <div className="content">
        <Switch>
          <Route path="/notes">
            <Notes user={user} setError={setError} />
          </Route>
          <Route path="/topics">
            <Topics />
          </Route>
          <Route path="/">
            <Home user={user} setUser={setUser} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
