const express = require('express');
const os = require('os');
const path = require('path');
// eslint-disable-next-line prefer-destructuring
const Cache = require('./cache').Cache;

const app = express();
const appCache = new Cache();

app.use(express.json());
app.use(express.static('dist'));

app.get('/api/getUsername', (req, res) => res.send({ username: os.userInfo().username }));

app.get('/api/getNotes', (req, res) => {
  if (!req.query.user) {
    res.status(400);
    res.send();
    return;
  }

  const notes = appCache.getValue(`${req.query.user}-notes`);
  res.send({ notes });
});

app.post('/api/setNote', (req, res) => {
  if (!req.body || !req.body.note || !req.query.user) {
    res.status(400);
    res.send();
    return;
  }

  const notes = appCache.getValue(`${req.query.user}-notes`);

  let noteFound = false;
  const updatedNotes = notes.map((note) => {
    if (note.id === req.body.note.id) {
      noteFound = true;
      return req.body.note;
    }

    return note;
  });
  if (!noteFound) {
    updatedNotes.push(req.body.note);
  }

  appCache.set({ [`${req.query.user}-notes`]: updatedNotes });
  res.status(200).send();
});

app.post('/api/setNotes', (req, res) => {
  if (!req.body || !req.body.notes || !req.query.user) {
    res.status(400);
    res.send();
    return;
  }

  appCache.set({ [`${req.query.user}-notes`]: req.body.notes });
  res.status(200).send();
});

app.post('/api/deleteNotes', (req, res) => {
  if (!req.query.user) {
    res.status(400);
    res.send();
    return;
  }

  appCache.delete(`${req.query.user}-notes`);
  res.status(200).send();
});

app.get('*', (req, res) => {
  res.sendFile(path.join(`${__dirname}/dist/index.html`));
});

app.listen(process.env.PORT || 8000, () => console.log(`Listening on port ${process.env.PORT || 8000}!`));
