const NodeCache = require('node-cache');

class Cache {
  myCache;

  constructor() {
    this.myCache = new NodeCache();
  }

  set(obj) {
    const objKeyValuePairs = [];
    Object.keys(obj).forEach(key => objKeyValuePairs.push({
      key,
      val: obj[key],
    }));

    this.myCache.mset(objKeyValuePairs);
  }

  getValue(key) {
    return this.myCache.get(key);
  }

  getAll() {
    return this.myCache.mget(this.myCache.keys());
  }

  delete(keys) {
    this.myCache.del(keys);
  }
}

exports.Cache = Cache;
